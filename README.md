# Todos Exercise

Features:

- adding/editing/removing todos, marking as complete

- filtering: all, complete, incomplete 

- todo search

- edited todos are automatically saved in local storage 

- todos reordering via drag and drop 

Tested for Chrome 72.0

## How to use
Clone the repository
```sh
git clone git@bitbucket.org:fsuchodolski/todos-fs.git
cd todos-fs
```

Install it and run
```sh
npm install
npm start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Available Scripts
### `npm test`

Launches the test runner in the interactive watch mode. See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder. 
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

---

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
