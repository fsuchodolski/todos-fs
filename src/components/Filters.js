import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import { ALL, COMPLETE, INCOMPLETE } from '../model/todoFilters';
import { statusFilters } from '../model/todoFilters';
import { getTrimmedValue } from '../utils/onChange';
import { compose, objOf } from 'ramda';
import getKeyDownHandler from '../utils/getKeyDownHandler';

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(0.4),
  },
  noRightMargin: {
    marginRight: 0,
  },
  queryInput: {
    width: '110px',
  },
}));

const Filters = ({ filters, onChange }) => {
  const classes = useStyles();
  const handleKeyDown = getKeyDownHandler({
    onEsc: () => onChange({ query: '' }),
  });

  return (
    <Grid container direction='row' justify='space-between'>
      <Grid item xs={4}>
        <TextField
          placeholder='Filter todos'
          value={filters.query}
          margin='dense'
          onChange={compose(onChange, objOf('query'), getTrimmedValue)}
          onKeyDown={handleKeyDown}
          InputProps={{
            startAdornment: <InputAdornment position='start'>
              <SearchIcon fontSize='small'/>
            </InputAdornment>,
          }}
        />
      </Grid>

      <Grid item container xs={8} justify='flex-end'>
        {statusFilters.map(({ status }, i, arr) => (
          <Button
            className={classnames({
              [classes.button]: true,
              [classes.noRightMargin]: i === arr.length - 1,
            })}
            value={status}
            key={status}
            size='small'
            variant={status === filters.status ? 'contained' : 'outlined'}
            color={status === filters.status ? 'primary' : undefined}
            onClick={() => onChange({ status })}
          >
            {status}
          </Button>
        ))}
      </Grid>
    </Grid>
  );
};

Filters.propTypes = {
  filters: PropTypes.shape({
    query: PropTypes.string,
    status: PropTypes.oneOf([ALL, COMPLETE, INCOMPLETE])
  }).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Filters;
