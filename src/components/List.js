import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';
import DraggableItem from './DraggableItem';
import Todo from './Todo';

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(2, 0),
  },
  listItem: {
    textAlign: 'left',
    margin: theme.spacing(0),
    height: '44px',
    borderBottom: 'rgba(0, 0, 0, 0.23) solid 1px',
  },
}));

const List = ({ todos, updateTodo, removeTodo }) => {
  const classes = useStyles();
  const [editedTodoId, setEditedTodoId] = useState(null);

  const onSave = (todo) => {
    setEditedTodoId(null);
    updateTodo(todo);
  };

  const onCancelEdit = () => {
    setEditedTodoId(null);
  };

  return (
    <div className={classes.root}>
      {
        todos.map((todo, index) => (
          <DraggableItem
            draggableId={todo.id}
            key={todo.id}
            index={index}
          >
            <Todo
              todo={todo}
              editProps={{
                save: onSave,
                cancel: onCancelEdit,
              }}
              isEdited={todo.id === editedTodoId}
              displayProps={{
                onChange: updateTodo,
                onRemoveClick: removeTodo,
                onEditClick: ({ id }) => setEditedTodoId(id)
              }}
            />
          </DraggableItem>
        ))
      }
    </div>
  );
};

List.propTypes = {
  todos: PropTypes.array.isRequired,
  updateTodo: PropTypes.func.isRequired,
  removeTodo: PropTypes.func.isRequired,
};

export default List;
