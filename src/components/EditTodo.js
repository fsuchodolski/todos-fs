import React from 'react';
import PropTypes from 'prop-types';
import InputBase from '@material-ui/core/InputBase';
import { makeStyles } from '@material-ui/styles';
import getKeyDownHandler from '../utils/getKeyDownHandler';

const useStyles = makeStyles({
  root: {
    textAlign: 'left',
    marginLeft: '12px',
  },
});

const EditTodo = ({ todo, save, cancel }) => {
  const classes = useStyles();

  const handleKeyDown = getKeyDownHandler({
    onEnter: text => save({ ...todo, text }),
    onEsc: () => cancel(),
  });

  return (
    <div className={classes.root}>
      <InputBase
        placeholder='Add something to do...'
        autoFocus
        onKeyDown={handleKeyDown}
        defaultValue={todo.text}
        margin='dense'
        onBlur={cancel}
      />
    </div>
  );
};

EditTodo.propTypes = {
  todo: PropTypes.object.isRequired,
  save: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
};

export default EditTodo;
