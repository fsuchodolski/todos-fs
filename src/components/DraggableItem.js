import React from 'react';
import PropTypes from 'prop-types';
import { Draggable } from 'react-beautiful-dnd';

class DraggableItem extends React.Component {
  render() {
    const { draggableId, index, children } = this.props;

    return (
      <Draggable draggableId={draggableId} index={index} key={draggableId}>
        {provided => (
          <div
            ref={provided.innerRef}
            {...{
              ...provided.draggableProps,
              ...provided.dragHandleProps
            }}>
            {children}
          </div>
        )}
      </Draggable>
    );
  }
}

DraggableItem.propTypes = {
  draggableId: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  children: PropTypes.any,
};

DraggableItem.defaultProps = {
  children: [],
};

export default DraggableItem;