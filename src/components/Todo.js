import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import EditTodo from './EditTodo';
import DisplayTodo from './DisplayTodo';

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(2, 0),
  },
  listItem: {
    textAlign: 'left',
    margin: theme.spacing(0),
    height: '44px',
    borderBottom: 'rgba(0, 0, 0, 0.23) solid 1px',
  },
}));

const Todo = ({ todo, editProps, displayProps, isEdited }) => {
  const classes = useStyles();

  return (
    <Grid container alignItems='flex-end' className={classes.listItem} key={todo.id}>
      {isEdited
        ? (<EditTodo todo={todo} {...editProps} />)
        : (<DisplayTodo todo={todo} {...displayProps} />)}
    </Grid>
  );
};

Todo.propTypes = {
  todo: PropTypes.object.isRequired,
  isEdited: PropTypes.bool.isRequired,
  editProps: PropTypes.object.isRequired,
  displayProps: PropTypes.object.isRequired,
};

export default Todo;
