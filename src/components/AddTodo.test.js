import React from 'react';
import { render, cleanup, getByDisplayValue, fireEvent } from 'react-testing-library';
import 'jest-dom/extend-expect';
import AddTodo from './AddTodo';
import events from '../utils/events';

afterEach(() => {
  cleanup();
  jest.clearAllMocks();
});

const testProps = {
  add: jest.fn(),
};

const getInput = () => {
  const { container } = render(<AddTodo {...testProps} />);
  return getByDisplayValue(container, '');
};

test('render input', () => {
  const input = getInput();
  expect(input).not.toBe(null);
});

test('type and add', () => {
  const input = getInput();
  fireEvent.change(input, events.value('new todo'));

  expect(input.value).toEqual('new todo');

  fireEvent.keyDown(input, events.enter);
  expect(testProps.add).toHaveBeenCalledWith({ text: 'new todo' });
});

test('type and cancel', () => {
  const input = getInput();
  fireEvent.change(input, events.value('entering todo...'));
  expect(input.value).toEqual('entering todo...');

  fireEvent.keyDown(input, events.esc);
  expect(input.value).toEqual('');
  expect(testProps.add).not.toHaveBeenCalled();
});
