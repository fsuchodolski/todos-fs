import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import { mergeLeft, compose } from 'ramda';
import AddTodo from './AddTodo';
import Filters from './Filters';
import { ALL, selectFilteredTodos } from '../model/todoFilters';
import DroppableContainer from './DroppableContainer';
import List from './List';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(0, 0, 1, 0),
    margin: theme.spacing(0, 0, 3, 0),
  },
}));

const TodosPage = ({ todos, addTodo, updateTodo, removeTodo, onDragEnd }) => {
  const classes = useStyles();
  const [filters, setFilters] = useState({ status: ALL, query: '' });
  const updateFilters = compose(setFilters, mergeLeft);
  const filteredTodos = selectFilteredTodos({ todos, filters });

  return (
    <Grid container spacing={1} className={classes.root}>
      <Grid item xs={12}>
        <AddTodo add={addTodo}/>
      </Grid>
      <Grid item xs={12}>
        <Filters filters={filters} onChange={updateFilters}/>
        <DroppableContainer onDragEnd={onDragEnd}>
          <List
            todos={filteredTodos}
            updateTodo={updateTodo}
            removeTodo={removeTodo}
          />
        </DroppableContainer>
        <span> Displaying {filteredTodos.length} out of {todos.length} todos. </span>
      </Grid>
    </Grid>
  );
};

TodosPage.propTypes = {
  todos: PropTypes.array.isRequired,
  addTodo: PropTypes.func.isRequired,
  updateTodo: PropTypes.func.isRequired,
  removeTodo: PropTypes.func.isRequired,
};

export default TodosPage;
