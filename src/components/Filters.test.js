import React from 'react';
import { render, cleanup, getByPlaceholderText, getByText, fireEvent } from 'react-testing-library';
import 'jest-dom/extend-expect';
import Filters from './Filters';
import { ALL, COMPLETE, INCOMPLETE } from '../model/todoFilters';
import events from '../utils/events';
import { assocPath } from 'ramda';
import { withMockTheme } from '../utils/testHelpers';

afterEach(() => {
  cleanup();
  jest.clearAllMocks();
});

const testProps = {
  filters: { query: '', status: ALL },
  onChange: jest.fn(),
};

const renderFilters = props => render(withMockTheme(
  <Filters {...{ ...testProps, ...props }} />
));

test('query filter', () => {
  const { container } = renderFilters();
  const queryInput = getByPlaceholderText(container, 'Filter todos');

  fireEvent.change(queryInput, events.value(' new query '));
  expect(testProps.onChange).toHaveBeenLastCalledWith({ query: 'new query' });

  fireEvent.keyDown(queryInput, events.esc);
  expect(testProps.onChange).toHaveBeenLastCalledWith({ query: '' });
});

test.each([
  [ALL, COMPLETE],
  [COMPLETE, ALL],
  [INCOMPLETE, ALL]
])(
  'status filter: %s',
  (clickedStatus, initStatus) => {
    const { container } = renderFilters({ ...assocPath(['filters', 'status'], initStatus, testProps) });
    const statusButton = getByText(container, clickedStatus);

    fireEvent.click(statusButton);
    expect(testProps.onChange).toHaveBeenLastCalledWith({ status: clickedStatus });
  },
);

