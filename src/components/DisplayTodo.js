import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';
import EditIcon from '@material-ui/icons/Edit';
import RemoveIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    textAlign: 'left',
    margin: theme.spacing(1, 0),
  },
  todoText: {
    alignSelf: 'center',
  },
  buttons: {
    marginLeft: 'auto',
  },
}));

const DisplayTodo = ({ todo, onChange, onEditClick, onRemoveClick }) => {
  const classes = useStyles();
  const { id } = todo;

  return (
    <Grid container alignContent='center'>
      <Checkbox
        data-testid='todo-checkbox'
        checked={todo.done}
        onChange={(ev, done) => onChange({ id, done })}
      />

      <Typography variant='body1' className={classes.todoText}>
        {todo.text}
      </Typography>

      <Grid item className={classes.buttons}>
        <IconButton aria-label='Edit' onClick={() => onEditClick({ id })}>
          <EditIcon fontSize='small'/>
        </IconButton>

        <IconButton aria-label='Remove' onClick={() => onRemoveClick({ id })}>
          <RemoveIcon fontSize='small'/>
        </IconButton>
      </Grid>
    </Grid>
  );
};

DisplayTodo.propTypes = {
  todo: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onEditClick: PropTypes.func.isRequired,
  onRemoveClick: PropTypes.func.isRequired,
};

export default DisplayTodo;
