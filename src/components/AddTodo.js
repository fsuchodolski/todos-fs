import React, { useState } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import getKeyDownHandler from '../utils/getKeyDownHandler';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
});

const AddTodo = ({ add }) => {
  const classes = useStyles();
  const initValue = '';
  const [value, setValue] = useState(initValue);
  const clearValue = () => setValue(initValue);

  const handleKeyDown = getKeyDownHandler({
    onEnter: text => {
      add({ text });
      clearValue();
    },
    onEsc: clearValue,
  });

  return (
    <TextField
      className={classes.root}
      autoFocus
      value={value}
      label='Add a task'
      margin='normal'
      type='text'
      variant='outlined'
      onKeyDown={handleKeyDown}
      onChange={ev => setValue(ev.target.value)}
      data-testid='add-todo'
    />
  );
};

AddTodo.propTypes = {
  add: PropTypes.func.isRequired,
};

export default AddTodo;
