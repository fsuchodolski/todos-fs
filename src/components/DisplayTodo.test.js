import React from 'react';
import { render, cleanup, getByTestId, getByLabelText, fireEvent, getByText } from 'react-testing-library';
import 'jest-dom/extend-expect';
import DisplayTodo from './DisplayTodo';
import { withMockTheme } from '../utils/testHelpers';
import { mergeRight } from 'ramda';

afterEach(cleanup);

const testProps = {
  todo: { id: 1, text: 'test todo', done: true },
  onChange: jest.fn(),
  onEditClick: jest.fn(),
  onRemoveClick: jest.fn(),
};

const renderTodo = (props = {}) => render(withMockTheme(
  <DisplayTodo {...mergeRight(testProps, props)}  />
));

describe('checkbox', () => {
  const getCheckbox = ({ done }) => {
    const { container } = renderTodo({ todo: { id: 1, text: 'checkbox test', done } });
    return getByTestId(container, 'todo-checkbox');
  };

  test('render checked', () => {
    const checkbox = getCheckbox({ done: true });
    expect(checkbox.getAttribute('class')).toContain('Mui-checked');
  });

  test('render unchecked', () => {
    const checkbox = getCheckbox({ done: false });
    expect(checkbox.getAttribute('class')).not.toContain('Mui-checked');
  });

  test('onChange is called on click', () => {
    const checkboxInput = getCheckbox({ done: false }).querySelector('input');

    fireEvent.click(checkboxInput);
    expect(testProps.onChange).toHaveBeenCalledWith({ done: true, id: 1 });
  });
});

test('label', () => {
  const { container } = renderTodo();
  expect(getByText(container, 'test todo')).not.toBeNull();
});

test('edit button', () => {
  const { container } = renderTodo();
  const editButton = getByLabelText(container, 'Edit');
  fireEvent.click(editButton);
  expect(testProps.onEditClick).toBeCalledWith({ id: 1 });
});

test('remove button', () => {
  const { container } = renderTodo();
  const removeButton = getByLabelText(container, 'Remove');
  fireEvent.click(removeButton);
  expect(testProps.onRemoveClick).toBeCalledWith({ id: 1 });
});
