import React from 'react';
import { render, cleanup, getByPlaceholderText, getByDisplayValue, fireEvent } from 'react-testing-library';
import 'jest-dom/extend-expect';
import EditTodo from './EditTodo';
import events from '../utils/events';

afterEach(cleanup);

const testProps = {
  todo: { id: 1, text: 'write tests' },
  save: jest.fn(),
  cancel: jest.fn(),
};

const getEditTodoInput = () => {
  const { container } = render(<EditTodo {...testProps} />);
  return getByPlaceholderText(container, 'Add something to do...');
};

test('render input with todo text', () => {
  const { container } = render(<EditTodo {...testProps} />);
  const input = getByDisplayValue(container, 'write tests');
  expect(input).not.toBe(null);
});

test('type and save', () => {
  const input = getEditTodoInput();
  fireEvent.change(input, events.value('edited todo'));

  expect(input.value).toEqual('edited todo');

  fireEvent.keyDown(input, events.enter);
  expect(testProps.save).toHaveBeenCalledWith({ id: 1, text: 'edited todo' });
  expect(testProps.cancel).not.toHaveBeenCalled();
});

test('cancel', () => {
  const input = getEditTodoInput();
  fireEvent.keyDown(input, events.esc);
  expect(testProps.cancel).toHaveBeenCalled();
});
