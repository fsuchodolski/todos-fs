import { compose, path, trim } from 'ramda';

export const getValue = path(['target', 'value']);
export const getTrimmedValue = compose(trim, getValue);
