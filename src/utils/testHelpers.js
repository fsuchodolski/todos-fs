import React from 'react';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme();

export const withMockTheme = component => (
  <ThemeProvider theme={theme}>
    {component}
  </ThemeProvider>
);