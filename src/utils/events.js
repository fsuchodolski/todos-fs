import { KEY_CODES } from '../model/consts';

export default {
  enter: { key: 'Enter', keyCode: KEY_CODES.ENTER },
  esc: { key: 'Esc', keyCode: KEY_CODES.ESC },
  value: value => ({ target: { value } }),
};