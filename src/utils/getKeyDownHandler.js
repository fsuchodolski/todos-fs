import { getTrimmedValue } from './onChange';
import { KEY_CODES } from '../model/consts';

export default ({ onEnter = () => {}, onEsc = () => {} }) => ev => {
  const value = getTrimmedValue(ev);

  switch (ev.keyCode) {
    case KEY_CODES.ENTER:
      if (value.length > 0) {
        onEnter(value);
      }
      return;
    case KEY_CODES.ESC:
      onEsc(value);
      return;
    default:
      return;
  }
};
