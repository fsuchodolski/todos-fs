import { connect } from 'react-redux';
import { compose, propEq, find } from 'ramda';
import TodosPage from '../components/TodosPage';
import { allTodos } from '../selectors';
import { addTodo, updateTodo, removeTodo, reorderTodos } from '../actions';
import createTodo from '../model/createTodo';

const mapStateToProps = (state) => ({
  todos: allTodos(state),
});

const mapDispatchToProps = {
  addTodo,
  updateTodo,
  removeTodo,
  reorderTodos,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  addTodo: compose(dispatchProps.addTodo, createTodo),
  onDragEnd: ({ destination, draggableId }) => {
    const movedTodo = find(propEq('id', draggableId), stateProps.todos);
    const firstPushedDownTodo = destination
      ? stateProps.todos[destination.index]
      : null;

    dispatchProps.reorderTodos({
      movedTodo,
      firstPushedDownTodo,
    });
  },
});

const TodosPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(TodosPage);

export default TodosPageContainer;
