import React from 'react';
import Container from '@material-ui/core/Container';
import { createMuiTheme } from '@material-ui/core/styles';
import { makeStyles, ThemeProvider } from '@material-ui/styles';
import { indigo, grey, red } from '@material-ui/core/colors';
import TodosPageContainer from './containers/TodosPageContainer';
import 'typeface-roboto';
import 'typeface-bitter';

const theme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: grey,
    error: red,
    contrastThreshold: 3,
    tonalOffset: 0.2,
  },
});

const useStyles = makeStyles({
  root: {
    background: theme.palette.secondary[100],
    height: '100%',
    minHeight: '100vh',
  },
  title: {
    textAlign: 'center',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    fontFamily: 'Bitter, serif',
    display: 'flex',
    height: '128px',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '68px',
  },
  container: {
    background: 'white',
    boxShadow: '0px 0px 18px 0px rgba(0,0,0,0.2)',
  }
});

function App() {
  const classes = useStyles();
  return (
    <ThemeProvider theme={theme}>
      <div className={classes.root}>
        <Container className={classes.container} maxWidth='sm'>
          <span className={classes.title}>
            Todos
          </span>
          <TodosPageContainer/>
        </Container>
      </div>
    </ThemeProvider>
  );
}

export default App;
