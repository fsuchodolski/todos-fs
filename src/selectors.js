import { createSelector } from 'reselect';
import { values, mapObjIndexed, compose, prop, sortBy } from 'ramda';
import { TODOS_STORE_KEY } from './model/consts';

export const allTodos = createSelector(
  prop(TODOS_STORE_KEY),
  compose(
    sortBy(prop('order')),
    values,
    mapObjIndexed((todo, id) => ({ ...todo, id }))
  ),
);
