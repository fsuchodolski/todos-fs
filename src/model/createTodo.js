import uniqid from 'uniqid';

export default init => ({
  id: uniqid(),
  done: false,
  ...init,
})