export const KEY_CODES = {
  ENTER: 13,
  ESC: 27,
};

export const TODOS_STORE_KEY = 'todos';
