import { assoc } from 'ramda';
import reorderTodos from './reorderTodos';

const setOrder = assoc('order');

const foo = { id: 'foo', order: 0 };
const bar = { id: 'bar', order: 1 };
const baz = { id: 'baz', order: 2 };

const testTodos = { foo, bar, baz };

test.each([
  [{ movedTodo: foo, firstPushedDownTodo: null, todos: testTodos },
    {
      bar: setOrder(0, bar),
      baz: setOrder(1, baz),
      foo: setOrder(2, foo),
    }
  ],
  [{ movedTodo: baz, firstPushedDownTodo: foo, todos: testTodos },
    {
      baz: setOrder(0, baz),
      foo: setOrder(1, foo),
      bar: setOrder(2, bar),
    }
  ],
  [{ movedTodo: bar, firstPushedDownTodo: baz, todos: testTodos },
    {
      foo: setOrder(0, foo),
      baz: setOrder(1, baz),
      bar: setOrder(2, bar),
    }
  ],
])(
  'todo reorder',
  (params, expected) => {
    expect(reorderTodos(params)).toEqual(expected);
  },
);
