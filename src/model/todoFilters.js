import { createSelector } from 'reselect';
import Fuse from 'fuse.js';
import { T, propEq, filter, find, path, prop } from 'ramda';

export const ALL = 'all';
export const COMPLETE = 'complete';
export const INCOMPLETE = 'incomplete';

export const statusFilters = [
  { status: ALL, predicate: T },
  { status: COMPLETE, predicate: propEq('done', true) },
  { status: INCOMPLETE, predicate: propEq('done', false) },
];

const fuseOptions = {
  shouldSort: false,
  threshold: 0.3,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: ['text'],
};

const selectTodosFilteredByStatus = createSelector(
  prop('todos'),
  path(['filters', 'status']),
  (todos, status) => {
    const { predicate } = find(propEq('status', status), statusFilters);
    return filter(predicate, todos);
  },
);

const selectFuse = createSelector(
  selectTodosFilteredByStatus,
  todos => (new Fuse(todos, fuseOptions)),
);

export const selectFilteredTodos = createSelector(
  selectFuse,
  path(['filters', 'query']),
  selectTodosFilteredByStatus,
  (fuse, query, todos) => query ? fuse.search(query) : todos,
);
