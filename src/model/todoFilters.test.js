import { ALL, COMPLETE, selectFilteredTodos, INCOMPLETE } from './todoFilters';

const todo1 = { id: 1, text: 'first todo', done: false };
const todo2 = { id: 2, text: 'second todo', done: true };
const todo3 = { id: 3, text: 'third', done: false };

const testTodos = [todo1, todo2, todo3];

test.each([
  [{ query: '', status: ALL }, testTodos, testTodos],
  [{ query: '', status: COMPLETE }, testTodos, [todo2]],
  [{ query: 'todo', status: ALL }, testTodos, [todo1, todo2]],
  [{ query: 'todo', status: COMPLETE }, testTodos, [todo2]],
  [{ query: 'todo', status: INCOMPLETE }, testTodos, [todo1]],
  [{ query: 'third', status: ALL }, testTodos, [todo3]],
  [{ query: 'no match', status: ALL }, testTodos, []],
])(
  'selectFilteredTodos for filters: %j',
  (filters, todos, expected) => {
    expect(selectFilteredTodos({ todos, filters })).toEqual(expected);
  },
);
