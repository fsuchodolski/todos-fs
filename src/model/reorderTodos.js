import { assoc, map, values, prop, defaultTo, compose, gte, lte, both, cond, T } from 'ramda';

export default ({ movedTodo, firstPushedDownTodo, todos }) => {
  const oldOrder = movedTodo.order;
  const maxOrder = values(todos).length - 1;
  const newOrder = compose(defaultTo(maxOrder), prop('order'))(firstPushedDownTodo);

  const getMoveValue = cond([
    [both(lte(oldOrder), gte(newOrder)), () => -1],
    [both(lte(newOrder), gte(oldOrder)), () => 1],
    [T, () => 0],
  ]);

  return map(todo => {
    const { order } = todo;

    const updatedOrder = movedTodo.id === todo.id
      ? newOrder
      : todo.order + getMoveValue(order);

    return assoc('order', updatedOrder, todo);
  }, todos);
}