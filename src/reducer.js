import { assoc, dissoc, values } from 'ramda';
import { combineReducers } from 'redux';
import { ADD_TODO, REMOVE_TODO, REORDER_TODOS, UPDATE_TODO } from './actionTypes';
import { TODOS_STORE_KEY } from './model/consts';
import reorderTodos from './model/reorderTodos';

const todosReducer = (state = {}, action) => {
  switch (action.type) {
    case ADD_TODO:
      const order = values(state).length;
      return assoc(action.todo.id, { ...action.todo, order }, state);
    case UPDATE_TODO:
      return assoc(action.todo.id, { ...state[action.todo.id], ...action.todo }, state);
    case REMOVE_TODO:
      return dissoc(action.todo.id, state);
    case REORDER_TODOS:
      const { movedTodo, firstPushedDownTodo } = action;
      return reorderTodos({ movedTodo, firstPushedDownTodo, todos: state });
    default:
      return state;
  }
};

export default combineReducers({
  [TODOS_STORE_KEY]: todosReducer,
});
