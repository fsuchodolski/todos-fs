export const ADD_TODO = 'todos/add';
export const UPDATE_TODO = 'todos/update';
export const REMOVE_TODO = 'todos/remove';
export const REORDER_TODOS = 'todos/reorder';
