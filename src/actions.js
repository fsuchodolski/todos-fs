import { ADD_TODO, REMOVE_TODO, REORDER_TODOS, UPDATE_TODO } from './actionTypes';

export const addTodo = (todo) => ({
  type: ADD_TODO,
  todo,
});

export const updateTodo = (todo) => ({
  type: UPDATE_TODO,
  todo,
});

export const removeTodo = (todo) => ({
  type: REMOVE_TODO,
  todo,
});

export const reorderTodos = ({ movedTodo, firstPushedDownTodo }) => ({
  type: REORDER_TODOS,
  movedTodo,
  firstPushedDownTodo,
});
